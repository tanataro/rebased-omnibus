#!/bin/sh

set -e

echo "-- Waiting for database..."
while ! pg_isready -d $DATABASE_URL -t 1; do
    sleep 1s
done

COUNT=$(psql -p 5433 -q -c "select count(datname) from pg_catalog.pg_database where datname = 'pleroma'" -t -A | tr -d '\n');

if [ "$COUNT" -lt "1" ]; then
    echo "-- Creating database..."
    psql -p 5433 -q -c "create user pleroma with encrypted password 'bleromer'"
    psql -p 5433 -q -c "create database pleroma with owner = 'pleroma'"
    psql -p 5433 -q -c "grant all privileges on database pleroma to pleroma"
fi

REPACK=$(psql -d pleroma -p 5433 -c "SELECT COUNT(*) FROM pg_extension WHERE extname='pg_repack';" -t -A | tr -d '\n')

if [ "$REPACK" -lt "1" ]; then
    echo "-- Enabling pg_repack..."
    set +e
    psql -d pleroma -p 5433 -c "CREATE EXTENSION pg_repack;"
    set -e
fi