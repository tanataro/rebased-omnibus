FROM ubuntu:22.04 as build

ADD "https://www.random.org/cgi-bin/randbyte?nbytes=10&format=h" skipcache

ARG MIX_ENV=prod

WORKDIR /src

RUN apt-get update &&\
    apt-get install --quiet -y git elixir erlang-dev erlang-nox build-essential cmake libssl-dev libmagic-dev automake autoconf libncurses5-dev curl &&\
    mix local.hex --force &&\
    mix local.rebar --force

RUN git clone https://gitlab.com/soapbox-pub/rebased.git /src

# .......................::^^~~!!!7777!!~^:.....
# ..................:^^~~!77??????JYYJJJJJ?7~:..
# ...............:^!!77!!!!!!7??J??JJJJ??JJJJ?^.
# ...........:^~~!7!77777!7777???J??JJJJ??7?JJJ~
# .........::^^^~~!!7???77?JJJ??JJ??JJJJJ???J55!
# ......:^~!77~:^^^~~!?JJJYYY5YYJJ?JJJYJYYYJJ?~:
# ....::^7?YYJJ???J5P?^7YYYYY5YYJJYY5YYJYY5J~:..
# ...:~!J5YJ??JP##BPJ!^~!YYYY5YJJ5YYYJJJJ?~:....
# ..^YPGPPGBBGYYPBBG55PP!7YY55JJJY55Y5Y?~:......
# :^~JY5PGBB#BPG##J^^~!?~:7Y55YYYY555?~:........
# .:^^!!777?JYPB#GJ777?J~.^JJJYYYYJ7~...........
# ....:^!!^~7!!Y5J!^!7??^.:?YYJJ7~:.............
# ............::::::::^7!~~?J7~:................

RUN rm lib/pleroma/web/activity_pub/mrf/remote_report_policy.ex; \
    rm test/pleroma/web/activity_pub/mrf/remote_report_policy_test.exs; \
	exit 0;

RUN mix deps.get --only prod &&\
    mkdir release &&\
    mix release --path release

FROM alpine:latest as soapbox

ADD "https://www.random.org/cgi-bin/randbyte?nbytes=10&format=h" skipcache

WORKDIR /tmp

RUN apk add curl unzip &&\
	mkdir /soapbox/ &&\
	curl -L "https://dl.soapbox.pub/main/soapbox.zip" -o soapbox.zip &&\
	unzip soapbox.zip -d /soapbox/

FROM ubuntu:22.04

ADD "https://www.random.org/cgi-bin/randbyte?nbytes=10&format=h" skipcache

ARG BUILD_DATE
ARG VCS_REF

ARG DEBIAN_FRONTEND="noninteractive"
ENV TZ="Etc/UTC"

ARG HOME=/opt/pleroma
ARG DATA=/var/lib/pleroma

RUN apt-get update &&\
    apt-get install --quiet -y --no-install-recommends imagemagick libmagic-dev ffmpeg libimage-exiftool-perl libncurses5 postgresql-client curl ca-certificates jq cron dumb-init fasttext &&\
    adduser --system --shell /bin/false --home ${HOME} pleroma &&\
    mkdir -p ${DATA}/uploads &&\
    mkdir -p ${DATA}/static &&\
    mkdir -p release &&\
    chown -R pleroma ${DATA} &&\
    mkdir -p /etc/pleroma &&\
    chown -R pleroma /etc/pleroma &&\
    mkdir -p /usr/share/fasttext &&\
    curl -L https://dl.fbaipublicfiles.com/fasttext/supervised-models/lid.176.ftz -o /usr/share/fasttext/lid.176.ftz &&\
    chmod 0644 /usr/share/fasttext/lid.176.ftz

# Caddy
ENV XDG_DATA_HOME /caddy/data
ENV XDG_CONFIG_HOME /caddy/config

RUN apt-get install -y debian-keyring debian-archive-keyring apt-transport-https curl gnupg &&\
    curl -1sLf 'https://dl.cloudsmith.io/public/caddy/stable/gpg.key' | gpg --dearmor -o /usr/share/keyrings/caddy-stable-archive-keyring.gpg &&\
    curl -1sLf 'https://dl.cloudsmith.io/public/caddy/stable/debian.deb.txt' | tee /etc/apt/sources.list.d/caddy-stable.list &&\
    apt-get update &&\
    apt-get install caddy &&\
    adduser --system --shell /bin/false --home /caddy caddy &&\
    mkdir /caddy && chown -R caddy:caddy /caddy

# PostgreSQL
RUN apt-get install -y postgresql-14 postgresql-client-14 postgresql-14-repack &&\
    mkdir /db &&\
    chown -R postgres:postgres /db

# Gosu
ENV GOSU_VERSION 1.14
RUN set -eux; \
# save list of currently installed packages for later so we can clean up
	savedAptMark="$(apt-mark showmanual)"; \
	apt-get update; \
	apt-get install -y --no-install-recommends ca-certificates wget; \
	if ! command -v gpg; then \
		apt-get install -y --no-install-recommends gnupg2 dirmngr; \
	elif gpg --version | grep -q '^gpg (GnuPG) 1\.'; then \
# "This package provides support for HKPS keyservers." (GnuPG 1.x only)
		apt-get install -y --no-install-recommends gnupg-curl; \
	fi; \
	rm -rf /var/lib/apt/lists/*; \
	\
	dpkgArch="$(dpkg --print-architecture | awk -F- '{ print $NF }')"; \
	wget -O /usr/local/bin/gosu "https://github.com/tianon/gosu/releases/download/$GOSU_VERSION/gosu-$dpkgArch"; \
	wget -O /usr/local/bin/gosu.asc "https://github.com/tianon/gosu/releases/download/$GOSU_VERSION/gosu-$dpkgArch.asc"; \
	\
# verify the signature
	export GNUPGHOME="$(mktemp -d)"; \
	gpg --batch --keyserver hkps://keys.openpgp.org --recv-keys B42F6819007F00F88E364FD4036A9C25BF357DD4; \
	gpg --batch --verify /usr/local/bin/gosu.asc /usr/local/bin/gosu; \
	command -v gpgconf && gpgconf --kill all || :; \
	rm -rf "$GNUPGHOME" /usr/local/bin/gosu.asc; \
	\
# clean up fetch dependencies
	apt-mark auto '.*' > /dev/null; \
	[ -z "$savedAptMark" ] || apt-mark manual $savedAptMark; \
	apt-get purge -y --auto-remove -o APT::AutoRemove::RecommendsImportant=false; \
	\
	chmod +x /usr/local/bin/gosu; \
# verify that the binary works
	gosu --version; \
	gosu nobody true

USER pleroma
COPY --from=build --chown=pleroma:0 /src/release ${HOME}
COPY ./docker.exs /etc/pleroma/config.exs
COPY --from=soapbox --chown=pleroma:0 /soapbox/ /var/lib/pleroma/static

USER root
RUN chown pleroma /etc/pleroma/config.exs && chmod 640 /etc/pleroma/config.exs

USER caddy
COPY ./Caddyfile /etc/Caddyfile

USER root
COPY ./docker-entrypoint.sh /run/docker-entrypoint.sh
RUN chmod 755 /run/docker-entrypoint.sh

COPY ./postgres.sh /run/postgres.sh
RUN chmod 755 /run/postgres.sh

COPY crontab /crontab.default
COPY postgresql.conf /postgresql.conf

VOLUME "/caddy"
VOLUME "/var/lib/postgresql/14/pleroma"
VOLUME "${DATA}/uploads"
VOLUME "${DATA}/static/instance"

EXPOSE 80
EXPOSE 443

ENTRYPOINT ["/run/docker-entrypoint.sh"]
