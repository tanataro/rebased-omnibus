# rebased-omnibus

Omnibus-style container image for Rebased. Alpha-quality, please don't use this in production yet!

### Components

- PostgreSQL 14
- Caddy
- Rebased
- Soapbox FE

## Build it

```
docker build . -t rebased-omnibus:latest
```

## Run it

First, point your domain name at the public IP of your server, so Caddy can get a proper TLS certificate. Then, boot up Docker:

```
docker run -it \
    -e DOMAIN=pl.your.domain \
    -e INSTANCE_NAME=Soapbox \
    -e ADMIN_EMAIL=me@your.domain \
    -e NOTIFY_EMAIL=notify@your.domain \
    -v /home/omnibus/caddy:/caddy \
    -v /home/omnibus/db:/var/lib/postgresql/14/pleroma \
    -v /home/omnibus/uploads:/var/lib/pleroma/uploads \
    -v /home/omnibus/static:/var/lib/pleroma/static \
    -p 0.0.0.0:80:80 \
    -p 0.0.0.0:443:443 \
    rebased-omnibus:latest
```

## License

&copy; 2022 Miss Pasture

&copy; 2021-2022 Soapbox and Rebased contributors

&copy; 2017-2022 Pleroma contributors

This is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version. This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details. You should have received a copy of the GNU Affero General Public License along with this software. If not, see https://www.gnu.org/licenses/.