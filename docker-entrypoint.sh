#!/usr/bin/dumb-init /bin/bash

set -e

##########
# Name resolution
##########

if [ -n "$IP" ]; then
	echo "Will check name resolution."
	resolv=$(curl "https://dns.google/resolve?name=$DOMAIN" | jq -c -r .Answer[0].data)
	while [ "$resolv" != "$IP" ]; do
		echo "Waiting for name to resolve properly (want $IP for $DOMAIN, got $resolv)..."
		sleep 60
		resolv=$(curl "https://dns.google/resolve?name=$DOMAIN" | jq -c -r .Answer[0].data)
	done
	echo "$DOMAIN resolves to $resolv"
fi

export DATABASE_URL="postgres://pleroma:bleromer@localhost:5433/pleroma"

##########
# PostgreSQL
##########

echo "-- Setting up PostgreSQL..."
chown -R postgres:postgres /var/lib/postgresql/14
chmod -R 750 /var/lib/postgresql/14

set +e
pg_createcluster 14 pleroma
cp /etc/postgresql/14/pleroma/*.conf /var/lib/postgresql/14/pleroma
cp /postgresql.conf /var/lib/postgresql/14/pleroma/postgresql.conf
set -e

pg_ctlcluster 14 pleroma start -- -t 3600
gosu postgres /run/postgres.sh

##########
# Caddy
##########

echo "-- Starting Caddy..."
chown -R caddy:caddy /caddy
sed -i -e "s/{{domain}}/$DOMAIN/" /etc/Caddyfile
gosu caddy caddy start --config /etc/Caddyfile

##########
# Migrations
##########

echo "-- Running migrations..."
gosu pleroma sh -c "/opt/pleroma/bin/pleroma_ctl migrate"

if [ ! -f "/metadata/migrated" ] && [ ! -f "/var/lib/pleroma/uploads/.migrated" ]; then
    echo "-- Transferring config to database"
    gosu pleroma sh -c "/opt/pleroma/bin/pleroma start" &
    sleep 10
    gosu pleroma sh -c "/opt/pleroma/bin/pleroma_ctl config migrate_to_db"

    if [ -n "ADMIN" ]; then
        echo "-- Creating admin user"
        set +e
        gosu pleroma sh -c "/opt/pleroma/bin/pleroma_ctl user new $ADMIN admin@$DOMAIN --password \"$PASSWORD\" --admin -y"
        set -e
        echo "-- Promoting admin user"
        # Work around soapbox bug
        gosu postgres sh -c "PGPASSWORD=bleromer psql -h localhost -p 5433 -q -U pleroma -c \"update users set is_admin='t' where name='$ADMIN';\""
    fi

    gosu pleroma sh -c "/opt/pleroma/bin/pleroma stop" &
    echo "migrated" > /metadata/migrated
    sleep 10
else
    echo "-- Already migrated config to DB. Skipping."
fi

if [ ! -f "/var/lib/pleroma/static/favicon.png" ]; then
    set +e
    echo "-- Symlinking favicon"
    ln -s /var/lib/pleroma/static/instance/favicon.png /var/lib/pleroma/static/favicon.png
    set -e
fi

if [ ! -d "/var/lib/pleroma/static/instance/about" ]; then
    echo "-- Creating instance about page directory"
    mkdir /var/lib/pleroma/static/instance/about
fi

if [ ! -d "/var/lib/pleroma/static/instance/emoji" ]; then
    echo "-- Creating emochichi target"
    mkdir /var/lib/pleroma/static/instance/emoji
    chown pleroma:root /var/lib/pleroma/static/instance/emoji
fi

if [ ! -d "/var/lib/pleroma/static/emoji" ]; then
    echo "-- Symlinking emochichi directory"

    ln -s /var/lib/pleroma/static/instance/emoji /var/lib/pleroma/static/emoji
fi

echo "-- Fixing file permissions..."
chown pleroma:root /var/lib/pleroma/uploads

if [ -n "$SHARED_SECRET" ] && [ -n "$REPORT_URL" ]; then
    echo "-- Setting up webhook..."
    gosu postgres psql -d pleroma -p 5433 -q -c "insert into webhooks (id, url, events, secret, enabled, inserted_at, updated_at) values (1, '$REPORT_URL', array ['report.created'], '$SHARED_SECRET', 't', now(), now()) on conflict (id) do nothing;"
fi

##########
# Cron
##########

NUMBERS=$((echo $(echo $IP | sed 's/\.//g') | sha512sum) | cut -c1-5)
DOM=$(($(printf "%d" 0x$(echo $NUMBERS | cut -c1)) + $(printf "%d" 0x$(echo $NUMBERS | cut -c2))))
HOD=$(($(($(printf "%d" 0x$(echo $NUMBERS | cut -c3)) + $(printf "%d" 0x$(echo $NUMBERS | cut -c4)) + $(printf "%d" 0x$(echo $NUMBERS | cut -c5)))) / 2))

if [[ "$DOM" -gt 30 ]]; then
    DOM=30
fi

echo "-- Scheduling cron for $DOM at $HOD ($NUMBERS)"

sed "s/DOM/$DOM/g" /crontab.default > /etc/crontab
sed "s/HOD/$HOD/g" /etc/crontab > /etc/crontab

echo "-- Starting cron..."
cron &

echo "-- Starting!"
gosu pleroma sh -c "/opt/pleroma/bin/pleroma start"
